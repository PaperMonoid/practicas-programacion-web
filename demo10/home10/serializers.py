from rest_framework import serializers
from . import models, forms


class PaqueteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Paquete
        fields = forms.PaqueteForm.Meta.fields
