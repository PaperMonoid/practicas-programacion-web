from django.db import models
import uuid
from django.core.validators import RegexValidator

name_validator = RegexValidator(
    regex=r"[a-zA-Z ]+", message="Must be alphanumeric", code="invalid"
)


class CustomManager(models.Manager):
    def first_three(self):
        return super().get_queryset()[:3]

    def first_five(self):
        return super().get_queryset()[:5]


class Paquete(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    nombre = models.CharField(max_length=30, validators=[name_validator])
    version = models.CharField(max_length=30)
    licencia = models.CharField(max_length=30)
    issues = models.IntegerField()
    pull_requests = models.IntegerField()
    sitio_web = models.CharField(max_length=30)
    ultima_actualizacion = models.DateField()
    descargas_semanales = models.IntegerField()

    objects = CustomManager()

    def __str__(self):
        return "{0} {1} {2}".format(
            self.ultima_actualizacion, self.nombre, self.version
        )

    def random(self):
        import rstr
        import datetime

        self.nombre = rstr.xeger(r"[A-Za-z\-]{1,30}")
        self.version = rstr.xeger(r"[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{1,2}")
        self.licencia = rstr.xeger(r"MIT|GPL|BSD")
        self.issues = int(str(rstr.xeger(r"[0-9]{3}")))
        self.pull_requests = int(str(rstr.xeger(r"[0-9]{3}")))
        self.sitio_web = rstr.xeger(r"github|gitlab|bitbucket")
        self.ultima_actualizacion = datetime.date.today()
        self.descargas_semanales = int(str(rstr.xeger(r"[0-9]{6}")))


from django.db.models.signals import pre_save, pre_delete
from django.dispatch import receiver


@receiver(pre_save, sender=Paquete)
def pre_save_handler(sender, instance, **kwargs):
    print("SAVING: {0}".format(instance))


@receiver(pre_delete, sender=Paquete)
def pre_delete_handler(sender, instance, **kwargs):
    print("DELETING: {0}".format(instance))


pre_save.connect(pre_save_handler, sender=Paquete)
pre_delete.connect(pre_delete_handler, sender=Paquete)
