from django.urls import path
from home10 import views

urlpatterns = [
    path("login", views.login_view, name="login"),
    path("logout", views.logout_view, name="logout"),
    path("signup", views.signup_view, name="signup"),
    path(
        "api/paquete_listview",
        views.paquete_listview.as_view(),
        name="api_paquete_listview",
    ),
    path(
        "api/paquete_createview",
        views.paquete_createview.as_view(),
        name="api_paquete_createview",
    ),
    path("api_client", views.api_client, name="api_client"),
    path("random_paquete", views.random_paquete, name="random_paquete"),
    path("1", views.template1, name="template1"),
    path("2", views.template2, name="template2"),
    path("3", views.template3, name="template3"),
    path("4", views.template4, name="template4"),
    path("5", views.template5, name="template5"),
    path("6", views.template6, name="template6"),
    path("7", views.template7, name="template7"),
    path("8", views.template8, name="template8"),
    path("9", views.template9, name="template9"),
    path("10", views.template10, name="template10"),
    path("paquete_crear", views.paquete_crear, name="paquete_crear"),
    path("paquete_editar/<str:id>", views.paquete_editar, name="paquete_editar"),
    path("paquete_eliminar/<str:id>", views.paquete_eliminar, name="paquete_eliminar"),
    path("paquete_lista", views.paquete_lista, name="paquete_lista"),
    path("paquete_detalles/<str:id>", views.paquete_detalles, name="paquete_detalles"),
    path(
        "generic/paquete_crear",
        views.generic_paquete_crear.as_view(),
        name="generic_paquete_crear",
    ),
    path(
        "generic/paquete_editar/<str:pk>",
        views.generic_paquete_editar.as_view(),
        name="generic_paquete_editar",
    ),
    path(
        "generic/paquete_eliminar/<str:pk>",
        views.generic_paquete_eliminar.as_view(),
        name="generic_paquete_eliminar",
    ),
    path(
        "generic/paquete_lista",
        views.generic_paquete_lista.as_view(),
        name="generic_paquete_lista",
    ),
    path(
        "generic/paquete_detalles/<str:pk>",
        views.generic_paquete_detalles.as_view(),
        name="generic_paquete_detalles",
    ),
]
