from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.http import HttpResponse
from rest_framework import generics
from . import models, forms, serializers

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

# Create your views here.
def login_view(request):
    if request.user.is_authenticated:
        return redirect("api_client")

    message = "Not Login"
    form = forms.LoginForm()

    if request.method == "POST":
        form = forms.LoginForm(request.POST or None)
        if form.is_valid():
            username = request.POST["username"]
            password = request.POST["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                message = "User logged"
                return redirect("api_client")
            else:
                message = "Username or password is not correct"
        else:
            message = "Invalid form"
    context = {"form": form, "message": message}
    return render(request, "login.html", context)


def logout_view(request):
    logout(request)
    return redirect("login")


def signup_view(request):
    if request.user.is_authenticated:
        return redirect("api_client")

    message = ""
    form = forms.SignupForm()

    if request.method == "POST":
        form = forms.SignupForm(request.POST or None)
        password = request.POST["password"]
        confirm_password = request.POST["confirm_password"]
        if password == confirm_password:
            if form.is_valid():
                user = User.objects.create_user(
                    username=request.POST["username"],
                    email=request.POST["email"],
                    first_name=request.POST["first_name"],
                    last_name=request.POST["last_name"],
                )
                user.set_password(password)
                user.save()
                user = authenticate(
                    request, username=request.POST["username"], password=password
                )
                if user is not None:
                    login(request, user)
                    return redirect("api_client")
                else:
                    message = "User not signup correctly"

            else:
                message = "Invalid form"
        else:
            message = "Passwords are not equals"
    context = {"form": form, "message": message}
    return render(request, "signup.html", context)


def api_client(request):
    return render(request, "home10/api-client.html", {"form": forms.PaqueteForm()})


def template1(request):
    return render(request, "home10/template-1.html", {})


def template2(request):
    return render(request, "home10/template-2.html", {})


def template3(request):
    return render(request, "home10/template-3.html", {})


def template4(request):
    return render(request, "home10/template-4.html", {})


def template5(request):
    return render(request, "home10/template-5.html", {})


def template6(request):
    return render(request, "home10/template-6.html", {})


def template7(request):
    return render(request, "home10/template-7.html", {})


def template8(request):
    return render(request, "home10/template-8.html", {})


def template9(request):
    return render(request, "home10/template-9.html", {})


def template10(request):
    return render(request, "home10/template-10.html", {})


def paquete_crear(request):
    form = forms.PaqueteForm(request.POST or None)
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy("paquete_lista"))
    contexto = {"forma": form}
    return render(request, "home10/paquete-crear.html", contexto)


from django.views.generic.edit import CreateView


class generic_paquete_crear(CreateView):
    model = models.Paquete
    success_url = reverse_lazy("paquete_lista")
    fields = forms.PaqueteForm.Meta.fields
    template_name = "home10/generic-paquete-crear.html"


class paquete_createview(generics.CreateAPIView):
    queryset = models.Paquete.objects.all()
    serializer_class = serializers.PaqueteSerializer


def paquete_editar(request, id):
    paquete = models.Paquete.objects.get(id=id)
    form = forms.PaqueteForm(request.POST or None, instance=paquete)
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy("paquete_lista"))
    contexto = {"forma": form}
    return render(request, "home10/paquete-crear.html", contexto)


from django.views.generic.edit import UpdateView


class generic_paquete_editar(UpdateView):
    model = models.Paquete
    success_url = reverse_lazy("paquete_lista")
    fields = forms.PaqueteForm.Meta.fields
    template_name = "home10/generic-paquete-crear.html"


def paquete_eliminar(request, id):
    paquete = models.Paquete.objects.get(id=id)
    if request.POST:
        paquete.delete()
        return redirect(reverse_lazy("paquete_lista"))
    contexto = {"paquete": paquete}
    return render(request, "home10/paquete-eliminar.html", contexto)


from django.views.generic.edit import DeleteView


class generic_paquete_eliminar(DeleteView):
    model = models.Paquete
    success_url = reverse_lazy("paquete_lista")
    template_name = "home10/generic-paquete-eliminar.html"


def paquete_lista(request):
    paquetes = models.Paquete.objects.all()
    if request.POST:
        paquetes = paquetes.filter(id=request.POST.get("palabra"))
    contexto = {"paquetes": paquetes}
    return render(request, "home10/paquete-lista.html", contexto)


from django.views.generic.list import ListView


class generic_paquete_lista(ListView):
    model = models.Paquete
    template_name = "home10/generic-paquete-lista.html"


class paquete_listview(generics.ListAPIView):
    queryset = models.Paquete.objects.all()
    serializer_class = serializers.PaqueteSerializer


def paquete_detalles(request, id):
    contexto = {"paquete": models.Paquete.objects.get(id=id)}
    return render(request, "home10/paquete-detalles.html", contexto)


from django.views.generic.detail import DetailView


class generic_paquete_detalles(DetailView):
    model = models.Paquete
    template_name = "home10/generic-paquete-detalles.html"


def random_paquete(request):
    for _ in range(0, 5):
        paquete = models.Paquete()
        paquete.random()
        paquete.save()
    return HttpResponse(status=200)
