from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.http import HttpResponse
from rest_framework import generics
from . import models, forms, serializers

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

# Create your views here.
def login_view(request):
    if request.user.is_authenticated:
        return redirect("api_client")

    message = "Not Login"
    form = forms.LoginForm()

    if request.method == "POST":
        form = forms.LoginForm(request.POST or None)
        if form.is_valid():
            username = request.POST["username"]
            password = request.POST["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                message = "User logged"
                return redirect("api_client")
            else:
                message = "Username or password is not correct"
        else:
            message = "Invalid form"
    context = {"form": form, "message": message}
    return render(request, "login.html", context)


def logout_view(request):
    logout(request)
    return redirect("login")


def signup_view(request):
    if request.user.is_authenticated:
        return redirect("api_client")

    message = ""
    form = forms.SignupForm()

    if request.method == "POST":
        form = forms.SignupForm(request.POST or None)
        password = request.POST["password"]
        confirm_password = request.POST["confirm_password"]
        if password == confirm_password:
            if form.is_valid():
                user = User.objects.create_user(
                    username=request.POST["username"],
                    email=request.POST["email"],
                    first_name=request.POST["first_name"],
                    last_name=request.POST["last_name"],
                )
                user.set_password(password)
                user.save()
                user = authenticate(
                    request, username=request.POST["username"], password=password
                )
                if user is not None:
                    login(request, user)
                    return redirect("api_client")
                else:
                    message = "User not signup correctly"

            else:
                message = "Invalid form"
        else:
            message = "Passwords are not equals"
    context = {"form": form, "message": message}
    return render(request, "signup.html", context)


def api_client(request):
    return render(request, "home6/api-client.html", {"form": forms.PeliculaForm()})


def template1(request):
    return render(request, "home6/template-1.html", {})


def template2(request):
    return render(request, "home6/template-2.html", {})


def template3(request):
    return render(request, "home6/template-3.html", {})


def template4(request):
    return render(request, "home6/template-4.html", {})


def template5(request):
    return render(request, "home6/template-5.html", {})


def template6(request):
    return render(request, "home6/template-6.html", {})


def pelicula_crear(request):
    form = forms.PeliculaForm(request.POST or None)
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy("pelicula_lista"))
    contexto = {"forma": form}
    return render(request, "home6/pelicula-crear.html", contexto)


from django.views.generic.edit import CreateView


class generic_pelicula_crear(CreateView):
    model = models.Pelicula
    success_url = reverse_lazy("pelicula_lista")
    fields = forms.PeliculaForm.Meta.fields
    template_name = "home6/generic-pelicula-crear.html"


class pelicula_createview(generics.CreateAPIView):
    queryset = models.Pelicula.objects.all()
    serializer_class = serializers.PeliculaSerializer


def pelicula_editar(request, id):
    pelicula = models.Pelicula.objects.get(id=id)
    form = forms.PeliculaForm(request.POST or None, instance=pelicula)
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy("pelicula_lista"))
    contexto = {"forma": form}
    return render(request, "home6/pelicula-crear.html", contexto)


from django.views.generic.edit import UpdateView


class generic_pelicula_editar(UpdateView):
    model = models.Pelicula
    success_url = reverse_lazy("pelicula_lista")
    fields = forms.PeliculaForm.Meta.fields
    template_name = "home6/generic-pelicula-crear.html"


def pelicula_eliminar(request, id):
    pelicula = models.Pelicula.objects.get(id=id)
    if request.POST:
        pelicula.delete()
        return redirect(reverse_lazy("pelicula_lista"))
    contexto = {"pelicula": pelicula}
    return render(request, "home6/pelicula-eliminar.html", contexto)


from django.views.generic.edit import DeleteView


class generic_pelicula_eliminar(DeleteView):
    model = models.Pelicula
    success_url = reverse_lazy("pelicula_lista")
    template_name = "home6/generic-pelicula-eliminar.html"


def pelicula_lista(request):
    peliculas = models.Pelicula.objects.all()
    if request.POST:
        peliculas = peliculas.filter(id=request.POST.get("palabra"))
    contexto = {"peliculas": peliculas}
    return render(request, "home6/pelicula-lista.html", contexto)


from django.views.generic.list import ListView


class generic_pelicula_lista(ListView):
    model = models.Pelicula
    template_name = "home6/generic-pelicula-lista.html"


class pelicula_listview(generics.ListAPIView):
    queryset = models.Pelicula.objects.all()
    serializer_class = serializers.PeliculaSerializer


def pelicula_detalles(request, id):
    contexto = {"pelicula": models.Pelicula.objects.get(id=id)}
    return render(request, "home6/pelicula-detalles.html", contexto)


from django.views.generic.detail import DetailView


class generic_pelicula_detalles(DetailView):
    model = models.Pelicula
    template_name = "home6/generic-pelicula-detalles.html"


def template7(request):
    return render(request, "home6/template-7.html", {})


def template8(request):
    return render(request, "home6/template-8.html", {})


def template9(request):
    return render(request, "home6/template-9.html", {})


def template10(request):
    return render(request, "home6/template-10.html", {})


def random_pelicula(request):
    for _ in range(0, 5):
        pelicula = models.Pelicula()
        pelicula.random()
        pelicula.save()

    return HttpResponse(status=200)
