from rest_framework import serializers
from . import models, forms


class PeliculaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Pelicula
        fields = forms.PeliculaForm.Meta.fields
