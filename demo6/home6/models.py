from django.db import models
import uuid
from django.core.validators import RegexValidator

name_validator = RegexValidator(
    regex=r"[a-zA-Z ]+", message="Must be alphanumeric", code="invalid"
)


class CustomManager(models.Manager):
    def first_three(self):
        return super().get_queryset()[:3]

    def first_five(self):
        return super().get_queryset()[:5]


class Pelicula(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    titulo = models.CharField(max_length=30)
    director = models.CharField(max_length=50, validators=[name_validator])
    productor = models.CharField(max_length=50, validators=[name_validator])
    musica_por = models.CharField(max_length=50)
    empresa_produccion = models.CharField(max_length=30)
    empresa_distribucion = models.CharField(max_length=30)
    fecha = models.DateField()
    duracion = models.IntegerField()
    pais = models.CharField(max_length=30)
    presupuesto = models.IntegerField()

    objects = CustomManager()

    def __str__(self):
        return "{0} {1}".format(self.fecha, self.titulo)

    def random(self):
        import rstr
        import datetime

        self.titulo = rstr.xeger(r"[A-Za-z ]{1,30}")
        self.director = rstr.xeger(r"[A-Za-z ]{1,50}")
        self.productor = rstr.xeger(r"[A-Za-z ]{1,50}")
        self.musica_por = rstr.xeger(r"[A-Za-z ]{1,50}")
        self.empresa_produccion = rstr.xeger(r"[A-Za-z ]{1,30}")
        self.empresa_distribucion = rstr.xeger(r"[A-Za-z ]{1,30}")
        self.fecha = datetime.date.today()
        self.duracion = int(str(rstr.xeger(r"[0-9]{2}")))
        self.pais = rstr.xeger(r"[A-Za-z]{1,30}")
        self.presupuesto = int(str(rstr.xeger(r"[0-9]{6}")))


from django.db.models.signals import pre_save, pre_delete
from django.dispatch import receiver


@receiver(pre_save, sender=Pelicula)
def pre_save_handler(sender, instance, **kwargs):
    print("SAVING: {0}".format(instance))


@receiver(pre_delete, sender=Pelicula)
def pre_delete_handler(sender, instance, **kwargs):
    print("DELETING: {0}".format(instance))


pre_save.connect(pre_save_handler, sender=Pelicula)
pre_delete.connect(pre_delete_handler, sender=Pelicula)
