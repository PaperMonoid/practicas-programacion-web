from django.urls import path
from home6 import views

urlpatterns = [
    path("login", views.login_view, name="login"),
    path("logout", views.logout_view, name="logout"),
    path("signup", views.signup_view, name="signup"),
    path(
        "api/pelicula_listview",
        views.pelicula_listview.as_view(),
        name="api_pelicula_listview",
    ),
    path(
        "api/pelicula_createview",
        views.pelicula_createview.as_view(),
        name="api_pelicula_createview",
    ),
    path("api_client", views.api_client, name="api_client"),
    path("random_pelicula", views.random_pelicula, name="random_pelicula"),
    path("1", views.template1, name="template1"),
    path("2", views.template2, name="template2"),
    path("3", views.template3, name="template3"),
    path("4", views.template4, name="template4"),
    path("5", views.template5, name="template5"),
    path("6", views.template6, name="template6"),
    path("pelicula_crear", views.pelicula_crear, name="pelicula_crear"),
    path("pelicula_editar/<str:id>", views.pelicula_editar, name="pelicula_editar"),
    path(
        "pelicula_eliminar/<str:id>", views.pelicula_eliminar, name="pelicula_eliminar"
    ),
    path("pelicula_lista", views.pelicula_lista, name="pelicula_lista"),
    path(
        "pelicula_detalles/<str:pk>", views.pelicula_detalles, name="pelicula_detalles"
    ),
    path(
        "generic/pelicula_crear",
        views.generic_pelicula_crear.as_view(),
        name="generic_pelicula_crear",
    ),
    path(
        "generic/pelicula_editar/<str:pk>",
        views.generic_pelicula_editar.as_view(),
        name="generic_pelicula_editar",
    ),
    path(
        "generic/pelicula_eliminar/<str:pk>",
        views.generic_pelicula_eliminar.as_view(),
        name="generic_pelicula_eliminar",
    ),
    path(
        "generic/pelicula_lista",
        views.generic_pelicula_lista.as_view(),
        name="generic_pelicula_lista",
    ),
    path(
        "generic/pelicula_detalles/<str:pk>",
        views.generic_pelicula_detalles.as_view(),
        name="generic_pelicula_detalles",
    ),
    path(
        "generic/pelicula_crear",
        views.generic_pelicula_crear.as_view(),
        name="generic_pelicula_crear",
    ),
    path(
        "generic/pelicula_editar/<str:pk>",
        views.generic_pelicula_editar.as_view(),
        name="generic_pelicula_editar",
    ),
    path(
        "generic/pelicula_eliminar/<str:pk>",
        views.generic_pelicula_eliminar.as_view(),
        name="generic_pelicula_eliminar",
    ),
    path(
        "generic/pelicula_lista",
        views.generic_pelicula_lista.as_view(),
        name="generic_pelicula_lista",
    ),
    path(
        "generic/pelicula_detalles/<str:pk>",
        views.generic_pelicula_detalles.as_view(),
        name="generic_pelicula_detalles",
    ),
    path("7", views.template7, name="template7"),
    path("8", views.template8, name="template8"),
    path("9", views.template9, name="template9"),
    path("10", views.template10, name="template10"),
]
