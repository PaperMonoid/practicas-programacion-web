from django.urls import path
from home2 import views

urlpatterns = [
    path("login", views.login_view, name="login"),
    path("logout", views.logout_view, name="logout"),
    path("signup", views.signup_view, name="signup"),
    path(
        "api/usuario_createview",
        views.usuario_createview.as_view(),
        name="api_usuario_createview",
    ),
    path(
        "api/usuario_listview",
        views.usuario_listview.as_view(),
        name="api_usuario_listview",
    ),
    path("api_client", views.api_client, name="api_client"),
    path("random_usuario", views.random_usuario, name="random_usuario"),
    path("usuario_crear", views.usuario_crear, name="usuario_crear"),
    path(
        "usuario_editar/<str:nombre_usuario>",
        views.usuario_editar,
        name="usuario_editar",
    ),
    path(
        "usuario_eliminar/<str:nombre_usuario>",
        views.usuario_eliminar,
        name="usuario_eliminar",
    ),
    path("usuario_lista", views.usuario_lista, name="usuario_lista"),
    path(
        "usuario_detalles/<str:nombre_usuario>",
        views.usuario_detalles,
        name="usuario_detalles",
    ),
    path(
        "generic/usuario_crear",
        views.generic_usuario_crear.as_view(),
        name="generic_usuario_crear",
    ),
    path(
        "generic/usuario_editar/<str:pk>",
        views.generic_usuario_editar.as_view(),
        name="generic_usuario_editar",
    ),
    path(
        "generic/usuario_eliminar/<str:pk>",
        views.generic_usuario_eliminar.as_view(),
        name="generic_usuario_eliminar",
    ),
    path(
        "generic/usuario_lista",
        views.generic_usuario_lista.as_view(),
        name="generic_usuario_lista",
    ),
    path(
        "generic/usuario_detalles/<str:pk>",
        views.generic_usuario_detalles.as_view(),
        name="generic_usuario_detalles",
    ),
    path("1", views.template1, name="template1"),
    path("2", views.template2, name="template2"),
    path("3", views.template3, name="template3"),
    path("4", views.template4, name="template4"),
    path("5", views.template5, name="template5"),
    path("6", views.template6, name="template6"),
    path("7", views.template7, name="template7"),
    path("8", views.template8, name="template8"),
    path("9", views.template9, name="template9"),
    path("10", views.template10, name="template10"),
]
