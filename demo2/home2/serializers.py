from rest_framework import serializers
from . import models, forms


class UsuarioSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Usuario
        fields = forms.UsuarioForm.Meta.fields
