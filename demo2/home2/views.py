from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.http import HttpResponse
from rest_framework import generics
from . import models, forms, serializers

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

# Create your views here.
def login_view(request):
    if request.user.is_authenticated:
        return redirect("api_client")

    message = "Not Login"
    form = forms.LoginForm()

    if request.method == "POST":
        form = forms.LoginForm(request.POST or None)
        if form.is_valid():
            username = request.POST["username"]
            password = request.POST["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                message = "User logged"
                return redirect("api_client")
            else:
                message = "Username or password is not correct"
        else:
            message = "Invalid form"
    context = {"form": form, "message": message}
    return render(request, "login.html", context)


def logout_view(request):
    logout(request)
    return redirect("login")


def signup_view(request):
    if request.user.is_authenticated:
        return redirect("api_client")

    message = ""
    form = forms.SignupForm()

    if request.method == "POST":
        form = forms.SignupForm(request.POST or None)
        password = request.POST["password"]
        confirm_password = request.POST["confirm_password"]
        if password == confirm_password:
            if form.is_valid():
                user = User.objects.create_user(
                    username=request.POST["username"],
                    email=request.POST["email"],
                    first_name=request.POST["first_name"],
                    last_name=request.POST["last_name"],
                )
                user.set_password(password)
                user.save()
                user = authenticate(
                    request, username=request.POST["username"], password=password
                )
                if user is not None:
                    login(request, user)
                    return redirect("api_client")
                else:
                    message = "User not signup correctly"

            else:
                message = "Invalid form"
        else:
            message = "Passwords are not equals"
    context = {"form": form, "message": message}
    return render(request, "signup.html", context)


def api_client(request):
    return render(request, "home2/api-client.html", {"form": forms.UsuarioForm()})


def template1(request):
    return render(request, "home2/template-1.html", {})


def template2(request):
    return render(request, "home2/template-2.html", {})


def usuario_crear(request):
    form = forms.UsuarioForm(request.POST or None)
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy("usuario_lista"))
    contexto = {"forma": form}
    return render(request, "home2/usuario-crear.html", contexto)


from django.views.generic.edit import CreateView


class generic_usuario_crear(CreateView):
    model = models.Usuario
    success_url = reverse_lazy("usuario_lista")
    fields = forms.UsuarioForm.Meta.fields
    template_name = "home2/generic-usuario-crear.html"


class usuario_createview(generics.CreateAPIView):
    queryset = models.Usuario.objects.all()
    serializer_class = serializers.UsuarioSerializer


def usuario_editar(request, nombre_usuario):
    usuario = models.Usuario.objects.get(nombre_usuario=nombre_usuario)
    form = forms.UsuarioForm(request.POST or None, instance=usuario)
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy("usuario_lista"))
    contexto = {"forma": form}
    return render(request, "home2/usuario-crear.html", contexto)


from django.views.generic.edit import UpdateView


class generic_usuario_editar(UpdateView):
    model = models.Usuario
    success_url = reverse_lazy("usuario_lista")
    fields = forms.UsuarioForm.Meta.fields
    template_name = "home2/generic-usuario-crear.html"


def usuario_eliminar(request, nombre_usuario):
    usuario = models.Usuario.objects.get(nombre_usuario=nombre_usuario)
    contexto = {"usuario": usuario}
    if request.POST:
        usuario.delete()
        return redirect(reverse_lazy("usuario_lista"))
    return render(request, "home2/usuario-eliminar.html", contexto)


from django.views.generic.edit import DeleteView


class generic_usuario_eliminar(DeleteView):
    model = models.Usuario
    success_url = reverse_lazy("usuario_lista")
    template_name = "home2/generic-usuario-eliminar.html"


def usuario_lista(request):
    usuarios = models.Usuario.objects.all()
    if request.POST:
        usuarios = usuarios.filter(nombre_usuario=request.POST.get("palabra"))
    contexto = {"usuarios": usuarios}
    return render(request, "home2/usuario-lista.html", contexto)


from django.views.generic.list import ListView


class generic_usuario_lista(ListView):
    model = models.Usuario
    template_name = "home2/generic-usuario-lista.html"


class usuario_listview(generics.ListAPIView):
    queryset = models.Usuario.objects.all()
    serializer_class = serializers.UsuarioSerializer


def usuario_detalles(request, nombre_usuario):
    contexto = {"usuario": models.Usuario.objects.get(nombre_usuario=nombre_usuario)}
    return render(request, "home2/usuario-detalles.html", contexto)


from django.views.generic.detail import DetailView


class generic_usuario_detalles(DetailView):
    model = models.Usuario
    template_name = "home2/generic-usuario-detalles.html"


def template3(request):
    return render(request, "home2/template-3.html", {})


def template4(request):
    return render(request, "home2/template-4.html", {})


def template5(request):
    return render(request, "home2/template-5.html", {})


def template6(request):
    return render(request, "home2/template-6.html", {})


def template7(request):
    return render(request, "home2/template-7.html", {})


def template8(request):
    return render(request, "home2/template-8.html", {})


def template9(request):
    return render(request, "home2/template-9.html", {})


def template10(request):
    return render(request, "home2/template-10.html", {})


def random_usuario(request):
    for _ in range(0, 5):
        usuario = models.Usuario()
        usuario.random()
        usuario.save()

    return HttpResponse(status=200)
