#!/bin/bash

for i in {1..10}
do
    virtualenv venv-$(echo $i)
    source venv-$(echo $i)/bin/activate
    pip install django
    django-admin startproject demo$(echo $i)
    deactivate
done

