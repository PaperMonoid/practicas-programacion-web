from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.http import HttpResponse
from rest_framework import generics
from . import models, forms, serializers

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

# Create your views here.
def login_view(request):
    if request.user.is_authenticated:
        return redirect("api_client")

    message = "Not Login"
    form = forms.LoginForm()

    if request.method == "POST":
        form = forms.LoginForm(request.POST or None)
        if form.is_valid():
            username = request.POST["username"]
            password = request.POST["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                message = "User logged"
                return redirect("api_client")
            else:
                message = "Username or password is not correct"
        else:
            message = "Invalid form"
    context = {"form": form, "message": message}
    return render(request, "login.html", context)


def logout_view(request):
    logout(request)
    return redirect("login")


def signup_view(request):
    if request.user.is_authenticated:
        return redirect("api_client")

    message = ""
    form = forms.SignupForm()

    if request.method == "POST":
        form = forms.SignupForm(request.POST or None)
        password = request.POST["password"]
        confirm_password = request.POST["confirm_password"]
        if password == confirm_password:
            if form.is_valid():
                user = User.objects.create_user(
                    username=request.POST["username"],
                    email=request.POST["email"],
                    first_name=request.POST["first_name"],
                    last_name=request.POST["last_name"],
                )
                user.set_password(password)
                user.save()
                user = authenticate(
                    request, username=request.POST["username"], password=password
                )
                if user is not None:
                    login(request, user)
                    return redirect("api_client")
                else:
                    message = "User not signup correctly"

            else:
                message = "Invalid form"
        else:
            message = "Passwords are not equals"
    context = {"form": form, "message": message}
    return render(request, "signup.html", context)


def api_client(request):
    return render(
        request, "home9/api-client.html", {"form": forms.SistemaOperativoForm()}
    )


def template1(request):
    return render(request, "home9/template-1.html", {})


def template2(request):
    return render(request, "home9/template-2.html", {})


def template3(request):
    return render(request, "home9/template-3.html", {})


def template4(request):
    return render(request, "home9/template-4.html", {})


def template5(request):
    return render(request, "home9/template-5.html", {})


def template6(request):
    return render(request, "home9/template-6.html", {})


def template7(request):
    return render(request, "home9/template-7.html", {})


def template8(request):
    return render(request, "home9/template-8.html", {})


def template9(request):
    return render(request, "home9/template-9.html", {})


def sistema_operativo_crear(request):
    form = forms.SistemaOperativoForm(request.POST or None)
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy("sistema_operativo_lista"))
    contexto = {"forma": form}
    return render(request, "home9/sistema-operativo-crear.html", contexto)


from django.views.generic.edit import CreateView


class generic_sistema_operativo_crear(CreateView):
    model = models.SistemaOperativo
    success_url = reverse_lazy("sistema_operativo_lista")
    fields = forms.SistemaOperativoForm.Meta.fields
    template_name = "home9/generic-sistema-operativo-crear.html"


class sistema_operativo_createview(generics.CreateAPIView):
    queryset = models.SistemaOperativo.objects.all()
    serializer_class = serializers.SistemaOperativoSerializer


def sistema_operativo_editar(request, id):
    sistema_operativo = models.SistemaOperativo.objects.get(id=id)
    form = forms.SistemaOperativoForm(request.POST or None, instance=sistema_operativo)
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy("sistema_operativo_lista"))
    contexto = {"forma": form}
    return render(request, "home9/sistema-operativo-crear.html", contexto)


from django.views.generic.edit import UpdateView


class generic_sistema_operativo_editar(UpdateView):
    model = models.SistemaOperativo
    success_url = reverse_lazy("sistema_operativo_lista")
    fields = forms.SistemaOperativoForm.Meta.fields
    template_name = "home9/generic-sistema-operativo-crear.html"


def sistema_operativo_eliminar(request, id):
    sistema_operativo = models.SistemaOperativo.objects.get(id=id)
    if request.POST:
        sistema_operativo.delete()
        return redirect(reverse_lazy("sistema_operativo_lista"))
    contexto = {"sistema_operativo": sistema_operativo}
    return render(request, "home9/sistema-operativo-eliminar.html", contexto)


from django.views.generic.edit import DeleteView


class generic_sistema_operativo_eliminar(DeleteView):
    model = models.SistemaOperativo
    success_url = reverse_lazy("sistema_operativo_lista")
    template_name = "home9/generic-sistema-operativo-eliminar.html"


def sistema_operativo_lista(request):
    sistemas_operativos = models.SistemaOperativo.objects.all()
    if request.POST:
        sistemas_operativos = sistemas_operativos.filter(id=request.POST.get("palabra"))
    contexto = {"sistemas_operativos": sistemas_operativos}
    return render(request, "home9/sistema-operativo-lista.html", contexto)


from django.views.generic.list import ListView


class generic_sistema_operativo_lista(ListView):
    model = models.SistemaOperativo
    template_name = "home9/generic-sistema-operativo-lista.html"


class sistema_operativo_listview(generics.ListAPIView):
    queryset = models.SistemaOperativo.objects.all()
    serializer_class = serializers.SistemaOperativoSerializer


def sistema_operativo_detalles(request, id):
    contexto = {"sistema_operativo": models.SistemaOperativo.objects.get(id=id)}
    return render(request, "home9/sistema-operativo-detalles.html", contexto)


from django.views.generic.detail import DetailView


class generic_sistema_operativo_detalles(DetailView):
    model = models.SistemaOperativo
    template_name = "home9/generic-sistema-operativo-detalles.html"


def template10(request):
    return render(request, "home9/template-10.html", {})


def random_sistema_operativo(request):
    for _ in range(0, 5):
        sistema_operativo = models.SistemaOperativo()
        sistema_operativo.random()
        sistema_operativo.save()

    return HttpResponse(status=200)
