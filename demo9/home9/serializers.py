from rest_framework import serializers
from . import models, forms


class SistemaOperativoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.SistemaOperativo
        fields = forms.SistemaOperativoForm.Meta.fields
