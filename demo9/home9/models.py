from django.db import models
import uuid
from django.core.validators import RegexValidator

name_validator = RegexValidator(
    regex=r"[a-zA-Z ]+", message="Must be alphanumeric", code="invalid"
)


class CustomManager(models.Manager):
    def first_three(self):
        return super().get_queryset()[:3]

    def first_five(self):
        return super().get_queryset()[:5]


class SistemaOperativo(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    nombre = models.CharField(max_length=30, validators=[name_validator])
    base = models.CharField(max_length=30)
    origen = models.CharField(max_length=30)
    arquitectura = models.CharField(max_length=15)
    categoria = models.CharField(max_length=15)
    escritorio = models.CharField(max_length=15)
    estatus = models.CharField(max_length=15)
    popularidad = models.IntegerField()
    rating = models.IntegerField()
    fecha = models.DateField()

    objects = CustomManager()

    def __str__(self):
        return "{0} {1}".format(self.fecha, self.nombre)

    def random(self):
        import rstr
        import datetime

        self.nombre = rstr.xeger(r"[A-Za-z ]{1,30}")
        self.base = rstr.xeger(r"Arch|Debian|Red hat")
        self.origen = rstr.xeger(r"[A-Za-z ]{1,30}")
        self.arquitectura = rstr.xeger(r"x86_64|i686")
        self.categoria = rstr.xeger(r"Escritorio|Live")
        self.escritorio = rstr.xeger(r"GNOME|XFCE")
        self.estatus = rstr.xeger(r"Activo|Inactivo")
        self.popularidad = int(str(rstr.xeger(r"[1-5]{1}")))
        self.rating = int(str(rstr.xeger(r"[1-5]{1}")))
        self.fecha = datetime.date.today()


from django.db.models.signals import pre_save, pre_delete
from django.dispatch import receiver


@receiver(pre_save, sender=SistemaOperativo)
def pre_save_handler(sender, instance, **kwargs):
    print("SAVING: {0}".format(instance))


@receiver(pre_delete, sender=SistemaOperativo)
def pre_delete_handler(sender, instance, **kwargs):
    print("DELETING: {0}".format(instance))


pre_save.connect(pre_save_handler, sender=SistemaOperativo)
pre_delete.connect(pre_delete_handler, sender=SistemaOperativo)
