from django.urls import path
from home9 import views

urlpatterns = [
    path("login", views.login_view, name="login"),
    path("logout", views.logout_view, name="logout"),
    path("signup", views.signup_view, name="signup"),
    path(
        "api/sistema_operativo_listview",
        views.sistema_operativo_listview.as_view(),
        name="api_sistema_operativo_listview",
    ),
    path(
        "api/sistema_operativo_createview",
        views.sistema_operativo_createview.as_view(),
        name="api_sistema_operativo_createview",
    ),
    path("api_client", views.api_client, name="api_client"),
    path(
        "random_sistema_operativo",
        views.random_sistema_operativo,
        name="random_sistema_operativo",
    ),
    path("1", views.template1, name="template1"),
    path("2", views.template2, name="template2"),
    path("3", views.template3, name="template3"),
    path("4", views.template4, name="template4"),
    path("5", views.template5, name="template5"),
    path("6", views.template6, name="template6"),
    path("7", views.template7, name="template7"),
    path("8", views.template8, name="template8"),
    path("9", views.template9, name="template9"),
    path(
        "sistema_operativo_crear",
        views.sistema_operativo_crear,
        name="sistema_operativo_crear",
    ),
    path(
        "sistema_operativo_editar/<str:id>",
        views.sistema_operativo_editar,
        name="sistema_operativo_editar",
    ),
    path(
        "sistema_operativo_eliminar/<str:id>",
        views.sistema_operativo_eliminar,
        name="sistema_operativo_eliminar",
    ),
    path(
        "sistema_operativo_lista",
        views.sistema_operativo_lista,
        name="sistema_operativo_lista",
    ),
    path(
        "sistema_operativo_detalles/<str:id>",
        views.sistema_operativo_detalles,
        name="sistema_operativo_detalles",
    ),
    path(
        "generic/sistema_operativo_crear",
        views.generic_sistema_operativo_crear.as_view(),
        name="generic_sistema_operativo_crear",
    ),
    path(
        "generic/sistema_operativo_editar/<str:pk>",
        views.generic_sistema_operativo_editar.as_view(),
        name="generic_sistema_operativo_editar",
    ),
    path(
        "generic/sistema_operativo_eliminar/<str:pk>",
        views.generic_sistema_operativo_eliminar.as_view(),
        name="generic_sistema_operativo_eliminar",
    ),
    path(
        "generic/sistema_operativo_lista",
        views.generic_sistema_operativo_lista.as_view(),
        name="generic_sistema_operativo_lista",
    ),
    path(
        "generic/sistema_operativo_detalles/<str:pk>",
        views.generic_sistema_operativo_detalles.as_view(),
        name="generic_sistema_operativo_detalles",
    ),
    path("10", views.template10, name="template10"),
]
