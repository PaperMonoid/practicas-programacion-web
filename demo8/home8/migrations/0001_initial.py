# Generated by Django 2.2.5 on 2019-09-17 06:04

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Cancion',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('titulo', models.CharField(max_length=30)),
                ('autor', models.CharField(max_length=50)),
                ('duracion', models.IntegerField()),
                ('fecha_lanzamiento', models.DateField()),
                ('version', models.CharField(max_length=15)),
                ('fecha_grabacion', models.DateField()),
                ('lenguaje', models.CharField(max_length=15)),
            ],
        ),
    ]
