from rest_framework import serializers
from . import models, forms


class CancionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Cancion
        fields = forms.CancionForm.Meta.fields
