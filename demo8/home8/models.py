from django.db import models
import uuid
from django.core.validators import RegexValidator

name_validator = RegexValidator(
    regex=r"[a-zA-Z ]+", message="Must be alphanumeric", code="invalid"
)


class CustomManager(models.Manager):
    def first_three(self):
        return super().get_queryset()[:3]

    def first_five(self):
        return super().get_queryset()[:5]


class Cancion(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    titulo = models.CharField(max_length=30)
    autor = models.CharField(max_length=50, validators=[name_validator])
    duracion = models.IntegerField()
    fecha_lanzamiento = models.DateField()
    version = models.CharField(max_length=15)
    fecha_grabacion = models.DateField()
    lenguaje = models.CharField(max_length=15)

    objects = CustomManager()

    def __str__(self):
        return "{0} {1} {2}".format(self.fecha_lanzamiento, self.autor, self.titulo)

    def random(self):
        import rstr
        import datetime

        self.titulo = rstr.xeger(r"[A-Za-z ]{1,30}")
        self.autor = rstr.xeger(r"[A-Za-z ]{1,50}")
        self.duracion = int(str(rstr.xeger(r"[0-9]{3}")))
        self.fecha_lanzamiento = datetime.date.today()
        self.version = rstr.xeger(r"Radio|Acústica")
        self.fecha_grabacion = datetime.date.today()
        self.lenguaje = rstr.xeger(r"Español|Inglés")


from django.db.models.signals import pre_save, pre_delete
from django.dispatch import receiver


@receiver(pre_save, sender=Cancion)
def pre_save_handler(sender, instance, **kwargs):
    print("SAVING: {0}".format(instance))


@receiver(pre_delete, sender=Cancion)
def pre_delete_handler(sender, instance, **kwargs):
    print("DELETING: {0}".format(instance))


pre_save.connect(pre_save_handler, sender=Cancion)
pre_delete.connect(pre_delete_handler, sender=Cancion)
