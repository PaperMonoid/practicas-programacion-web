from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.http import HttpResponse
from rest_framework import generics
from . import models, forms, serializers

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

# Create your views here.
def login_view(request):
    if request.user.is_authenticated:
        return redirect("api_client")

    message = "Not Login"
    form = forms.LoginForm()

    if request.method == "POST":
        form = forms.LoginForm(request.POST or None)
        if form.is_valid():
            username = request.POST["username"]
            password = request.POST["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                message = "User logged"
                return redirect("api_client")
            else:
                message = "Username or password is not correct"
        else:
            message = "Invalid form"
    context = {"form": form, "message": message}
    return render(request, "login.html", context)


def logout_view(request):
    logout(request)
    return redirect("login")


def signup_view(request):
    if request.user.is_authenticated:
        return redirect("api_client")

    message = ""
    form = forms.SignupForm()

    if request.method == "POST":
        form = forms.SignupForm(request.POST or None)
        password = request.POST["password"]
        confirm_password = request.POST["confirm_password"]
        if password == confirm_password:
            if form.is_valid():
                user = User.objects.create_user(
                    username=request.POST["username"],
                    email=request.POST["email"],
                    first_name=request.POST["first_name"],
                    last_name=request.POST["last_name"],
                )
                user.set_password(password)
                user.save()
                user = authenticate(
                    request, username=request.POST["username"], password=password
                )
                if user is not None:
                    login(request, user)
                    return redirect("api_client")
                else:
                    message = "User not signup correctly"

            else:
                message = "Invalid form"
        else:
            message = "Passwords are not equals"
    context = {"form": form, "message": message}
    return render(request, "signup.html", context)


def api_client(request):
    return render(request, "home8/api-client.html", {"form": forms.CancionForm()})


def template1(request):
    return render(request, "home8/template-1.html", {})


def template2(request):
    return render(request, "home8/template-2.html", {})


def template3(request):
    return render(request, "home8/template-3.html", {})


def template4(request):
    return render(request, "home8/template-4.html", {})


def template5(request):
    return render(request, "home8/template-5.html", {})


def template6(request):
    return render(request, "home8/template-6.html", {})


def template7(request):
    return render(request, "home8/template-7.html", {})


def template8(request):
    return render(request, "home8/template-8.html", {})


def cancion_crear(request):
    form = forms.CancionForm(request.POST or None)
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy("cancion_lista"))
    contexto = {"forma": form}
    return render(request, "home8/cancion-crear.html", contexto)


from django.views.generic.edit import CreateView


class generic_cancion_crear(CreateView):
    model = models.Cancion
    success_url = reverse_lazy("cancion_lista")
    fields = forms.CancionForm.Meta.fields
    template_name = "home8/generic-cancion-crear.html"


class cancion_createview(generics.CreateAPIView):
    queryset = models.Cancion.objects.all()
    serializer_class = serializers.CancionSerializer


def cancion_editar(request, id):
    cancion = models.Cancion.objects.get(id=id)
    form = forms.CancionForm(request.POST or None, instance=cancion)
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy("cancion_lista"))
    contexto = {"forma": form}
    return render(request, "home8/cancion-crear.html", contexto)


from django.views.generic.edit import UpdateView


class generic_cancion_editar(UpdateView):
    model = models.Cancion
    success_url = reverse_lazy("cancion_lista")
    fields = forms.CancionForm.Meta.fields
    template_name = "home8/generic-cancion-crear.html"


def cancion_eliminar(request, id):
    cancion = models.Cancion.objects.get(id=id)
    if request.POST:
        cancion.delete()
        return redirect(reverse_lazy("cancion_lista"))
    contexto = {"cancion": cancion}
    return render(request, "home8/cancion-eliminar.html", contexto)


from django.views.generic.edit import DeleteView


class generic_cancion_eliminar(DeleteView):
    model = models.Cancion
    success_url = reverse_lazy("cancion_lista")
    template_name = "home8/generic-cancion-eliminar.html"


def cancion_lista(request):
    canciones = models.Cancion.objects.all()
    if request.POST:
        canciones = canciones.filter(id=request.POST.get("palabra"))
    contexto = {"canciones": canciones}
    return render(request, "home8/cancion-lista.html", contexto)


from django.views.generic.list import ListView


class generic_cancion_lista(ListView):
    model = models.Cancion
    template_name = "home8/generic-cancion-lista.html"


class cancion_listview(generics.ListAPIView):
    queryset = models.Cancion.objects.all()
    serializer_class = serializers.CancionSerializer


def cancion_detalles(request, id):
    contexto = {"cancion": models.Cancion.objects.get(id=id)}
    return render(request, "home8/cancion-detalles.html", contexto)


from django.views.generic.detail import DetailView


class generic_cancion_detalles(DetailView):
    model = models.Cancion
    template_name = "home8/generic-cancion-detalles.html"


def template9(request):
    return render(request, "home8/template-9.html", {})


def template10(request):
    return render(request, "home8/template-10.html", {})


def random_cancion(request):
    for _ in range(0, 5):
        cancion = models.Cancion()
        cancion.random()
        cancion.save()

    return HttpResponse(status=200)
