from django.urls import path
from home8 import views

urlpatterns = [
    path("login", views.login_view, name="login"),
    path("logout", views.logout_view, name="logout"),
    path("signup", views.signup_view, name="signup"),
    path(
        "api/cancion_listview",
        views.cancion_listview.as_view(),
        name="api_cancion_listview",
    ),
    path(
        "api/cancion_createview",
        views.cancion_createview.as_view(),
        name="api_cancion_createview",
    ),
    path("api_client", views.api_client, name="api_client"),
    path("random_cancion", views.random_cancion, name="random_cancion"),
    path("1", views.template1, name="template1"),
    path("2", views.template2, name="template2"),
    path("3", views.template3, name="template3"),
    path("4", views.template4, name="template4"),
    path("5", views.template5, name="template5"),
    path("6", views.template6, name="template6"),
    path("7", views.template7, name="template7"),
    path("8", views.template8, name="template8"),
    path("cancion_crear", views.cancion_crear, name="cancion_crear"),
    path("cancion_editar/<str:id>", views.cancion_editar, name="cancion_editar"),
    path("cancion_eliminar/<str:id>", views.cancion_eliminar, name="cancion_eliminar"),
    path("cancion_lista", views.cancion_lista, name="cancion_lista"),
    path("cancion_detalles/<str:id>", views.cancion_detalles, name="cancion_detalles"),
    path(
        "generic/cancion_crear",
        views.generic_cancion_crear.as_view(),
        name="generic_cancion_crear",
    ),
    path(
        "generic/cancion_editar/<str:pk>",
        views.generic_cancion_editar.as_view(),
        name="generic_cancion_editar",
    ),
    path(
        "generic/cancion_eliminar/<str:pk>",
        views.generic_cancion_eliminar.as_view(),
        name="generic_cancion_eliminar",
    ),
    path(
        "generic/cancion_lista",
        views.generic_cancion_lista.as_view(),
        name="generic_cancion_lista",
    ),
    path(
        "generic/cancion_detalles/<str:pk>",
        views.generic_cancion_detalles.as_view(),
        name="generic_cancion_detalles",
    ),
    path("9", views.template9, name="template9"),
    path("10", views.template10, name="template10"),
]
