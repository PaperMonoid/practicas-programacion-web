from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.http import HttpResponse
from rest_framework import generics
from . import models, forms, serializers

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

# Create your views here.
def login_view(request):
    if request.user.is_authenticated:
        return redirect("api_client")

    message = "Not Login"
    form = forms.LoginForm()

    if request.method == "POST":
        form = forms.LoginForm(request.POST or None)
        if form.is_valid():
            username = request.POST["username"]
            password = request.POST["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                message = "User logged"
                return redirect("api_client")
            else:
                message = "Username or password is not correct"
        else:
            message = "Invalid form"
    context = {"form": form, "message": message}
    return render(request, "login.html", context)


def logout_view(request):
    logout(request)
    return redirect("login")


def signup_view(request):
    if request.user.is_authenticated:
        return redirect("api_client")

    message = ""
    form = forms.SignupForm()

    if request.method == "POST":
        form = forms.SignupForm(request.POST or None)
        password = request.POST["password"]
        confirm_password = request.POST["confirm_password"]
        if password == confirm_password:
            if form.is_valid():
                user = User.objects.create_user(
                    username=request.POST["username"],
                    email=request.POST["email"],
                    first_name=request.POST["first_name"],
                    last_name=request.POST["last_name"],
                )
                user.set_password(password)
                user.save()
                user = authenticate(
                    request, username=request.POST["username"], password=password
                )
                if user is not None:
                    login(request, user)
                    return redirect("api_client")
                else:
                    message = "User not signup correctly"

            else:
                message = "Invalid form"
        else:
            message = "Passwords are not equals"
    context = {"form": form, "message": message}
    return render(request, "signup.html", context)


def api_client(request):
    return render(request, "home/api-client.html", {"form": forms.AlumnoForm()})


def template1(request):
    return render(request, "home/template-1.html", {})


def alumno_crear(request):
    form = forms.AlumnoForm(request.POST or None)
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy("alumno_lista"))
    contexto = {"forma": form}
    return render(request, "home/alumno-crear.html", contexto)


from django.views.generic.edit import CreateView


class generic_alumno_crear(CreateView):
    model = models.Alumno
    success_url = reverse_lazy("alumno_lista")
    fields = forms.AlumnoForm.Meta.fields
    template_name = "home/generic-alumno-crear.html"


class alumno_createview(generics.CreateAPIView):
    queryset = models.Alumno.objects.all()
    serializer_class = serializers.AlumnoSerializer


def alumno_editar(request, numero_control):
    alumno = models.Alumno.objects.get(numero_control=numero_control)
    form = forms.AlumnoForm(request.POST or None, instance=alumno)
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy("alumno_lista"))
    contexto = {"forma": form}
    return render(request, "home/alumno-crear.html", contexto)


from django.views.generic.edit import UpdateView


class generic_alumno_editar(UpdateView):
    model = models.Alumno
    success_url = reverse_lazy("alumno_lista")
    fields = forms.AlumnoForm.Meta.fields
    template_name = "home/generic-alumno-crear.html"


def alumno_eliminar(request, numero_control):
    alumno = models.Alumno.objects.get(numero_control=numero_control)
    contexto = {"alumno": alumno}
    if request.POST:
        alumno.delete()
        return redirect(reverse_lazy("alumno_lista"))
    return render(request, "home/alumno-eliminar.html", contexto)


from django.views.generic.edit import DeleteView


class generic_alumno_eliminar(DeleteView):
    model = models.Alumno
    success_url = reverse_lazy("alumno_lista")
    template_name = "home/generic-alumno-eliminar.html"


def alumno_lista(request):
    alumnos = models.Alumno.objects.all()
    if request.POST:
        alumnos = alumnos.filter(numero_control=request.POST.get("palabra"))
    contexto = {"alumnos": alumnos}
    return render(request, "home/alumno-lista.html", contexto)


from django.views.generic.list import ListView


class generic_alumno_lista(ListView):
    model = models.Alumno
    template_name = "home/generic-alumno-lista.html"


class alumno_listview(generics.ListAPIView):
    queryset = models.Alumno.objects.all()
    serializer_class = serializers.AlumnoSerializer


def alumno_detalles(request, numero_control):
    contexto = {"alumno": models.Alumno.objects.get(numero_control=numero_control)}
    return render(request, "home/alumno-detalles.html", contexto)


from django.views.generic.detail import DetailView


class generic_alumno_detalles(DetailView):
    model = models.Alumno
    template_name = "home/generic-alumno-detalles.html"


def template2(request):
    return render(request, "home/template-2.html", {})


def template3(request):
    return render(request, "home/template-3.html", {})


def template4(request):
    return render(request, "home/template-4.html", {})


def template5(request):
    return render(request, "home/template-5.html", {})


def template6(request):
    return render(request, "home/template-6.html", {})


def template7(request):
    return render(request, "home/template-7.html", {})


def template8(request):
    return render(request, "home/template-8.html", {})


def template9(request):
    return render(request, "home/template-9.html", {})


def template10(request):
    return render(request, "home/template-10.html", {})


def random_alumno(request):
    for _ in range(0, 5):
        alumno = models.Alumno()
        alumno.random()
        alumno.save()

    return HttpResponse(status=200)
