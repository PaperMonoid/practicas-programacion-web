from django.urls import path
from home import views

urlpatterns = [
    path("login", views.login_view, name="login"),
    path("logout", views.logout_view, name="logout"),
    path("signup", views.signup_view, name="signup"),
    path(
        "api/alumno_listview",
        views.alumno_listview.as_view(),
        name="api_alumno_listview",
    ),
    path(
        "api/alumno_createview",
        views.alumno_createview.as_view(),
        name="api_alumno_createview",
    ),
    path("api_client", views.api_client, name="api_client"),
    path("random_alumno", views.random_alumno, name="random_alumno"),
    path("1", views.template1, name="template1"),
    path("alumno_crear", views.alumno_crear, name="alumno_crear"),
    path(
        "alumno_editar/<int:numero_control>", views.alumno_editar, name="alumno_editar"
    ),
    path(
        "alumno_eliminar/<int:numero_control>",
        views.alumno_eliminar,
        name="alumno_eliminar",
    ),
    path("alumno", views.alumno_lista, name="alumno_lista"),
    path("alumno/<int:pk>", views.alumno_detalles, name="alumno_detalles"),
    path(
        "generic/alumno_crear",
        views.generic_alumno_crear.as_view(),
        name="generic_alumno_crear",
    ),
    path(
        "generic/alumno_editar/<int:pk>",
        views.generic_alumno_editar.as_view(),
        name="generic_alumno_editar",
    ),
    path(
        "generic/alumno_eliminar/<int:pk>",
        views.generic_alumno_eliminar.as_view(),
        name="generic_alumno_eliminar",
    ),
    path(
        "generic/alumno",
        views.generic_alumno_lista.as_view(),
        name="generic_alumno_lista",
    ),
    path(
        "generic/alumno/<int:pk>",
        views.generic_alumno_detalles.as_view(),
        name="generic_alumno_detalles",
    ),
    path("2", views.template2, name="template2"),
    path("3", views.template3, name="template3"),
    path("4", views.template4, name="template4"),
    path("5", views.template5, name="template5"),
    path("6", views.template6, name="template6"),
    path("7", views.template7, name="template7"),
    path("8", views.template8, name="template8"),
    path("9", views.template9, name="template9"),
    path("10", views.template10, name="template10"),
]
