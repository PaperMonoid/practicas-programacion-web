from rest_framework import serializers
from . import models, forms


class AlumnoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Alumno
        fields = forms.AlumnoForm.Meta.fields
