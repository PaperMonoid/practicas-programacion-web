from django.db import models
from django.core.validators import RegexValidator, EmailValidator

name_validator = RegexValidator(
    regex=r"[a-zA-Z ]+", message="Must be alphanumeric", code="invalid"
)
email_validator = EmailValidator(
    whitelist="gmail", message="Must be gmail", code="invalid"
)


class CustomManager(models.Manager):
    def first_three(self):
        return super().get_queryset()[:3]

    def first_five(self):
        return super().get_queryset()[:5]


class Alumno(models.Model):
    nombre = models.CharField(max_length=30, validators=[name_validator])
    apellidos = models.CharField(max_length=30, validators=[name_validator])
    genero = models.BooleanField()
    curp = models.CharField(max_length=50)
    correo_electronico = models.CharField(max_length=50, validators=[email_validator])
    telefono_domicilio = models.CharField(max_length=12)
    telefono_celular = models.CharField(max_length=12)
    numero_control = models.CharField(primary_key=True, max_length=8)
    carrera = models.CharField(max_length=5)
    semestre = models.IntegerField()

    objects = CustomManager()

    def __str__(self):
        return "{0} {1} {2}".format(self.numero_control, self.apellidos, self.nombre)

    def random(self):
        import rstr

        self.nombre = rstr.xeger(r"[A-Za-z]{3,15}")
        self.apellidos = rstr.xeger(r"[A-Za-z]{3,15}")
        self.genero = int(rstr.xeger(r"[01]"))
        self.curp = rstr.xeger(r"[A-Za-z0-9]{15}")
        self.correo_electronico = rstr.xeger(r"[A-Za-z0-9]{0,15}@[A-Za-z]{0,10}\.com")
        self.telefono_domicilio = rstr.xeger(r"[0-9]{12}")
        self.telefono_celular = rstr.xeger(r"[0-9]{12}")
        self.numero_control = rstr.xeger(r"[0-9]{8}")
        self.carrera = rstr.xeger(r"ISC|TIC|INF|IND")
        self.semestre = int(str(rstr.xeger(r"[1-9]")))


from django.db.models.signals import pre_save, pre_delete
from django.dispatch import receiver


@receiver(pre_save, sender=Alumno)
def pre_save_handler(sender, instance, **kwargs):
    print("SAVING: {0}".format(instance))


@receiver(pre_delete, sender=Alumno)
def pre_delete_handler(sender, instance, **kwargs):
    print("DELETING: {0}".format(instance))


pre_save.connect(pre_save_handler, sender=Alumno)
pre_delete.connect(pre_delete_handler, sender=Alumno)
