from rest_framework import serializers
from . import models, forms


class LibroSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Libro
        fields = forms.LibroForm.Meta.fields
