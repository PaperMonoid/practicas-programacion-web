from django.urls import path
from home7 import views

urlpatterns = [
    path("login", views.login_view, name="login"),
    path("logout", views.logout_view, name="logout"),
    path("signup", views.signup_view, name="signup"),
    path(
        "api/libro_listview", views.libro_listview.as_view(), name="api_libro_listview"
    ),
    path(
        "api/libro_createview",
        views.libro_createview.as_view(),
        name="api_libro_createview",
    ),
    path("api_client", views.api_client, name="api_client"),
    path("random_libro", views.random_libro, name="random_libro"),
    path("1", views.template1, name="template1"),
    path("2", views.template2, name="template2"),
    path("3", views.template3, name="template3"),
    path("4", views.template4, name="template4"),
    path("5", views.template5, name="template5"),
    path("6", views.template6, name="template6"),
    path("7", views.template7, name="template7"),
    path("libro_crear", views.libro_crear, name="libro_crear"),
    path("libro_editar/<str:id>", views.libro_editar, name="libro_editar"),
    path("libro_eliminar/<str:id>", views.libro_eliminar, name="libro_eliminar"),
    path("libro_lista", views.libro_lista, name="libro_lista"),
    path("libro_detalles/<str:id>", views.libro_detalles, name="libro_detalles"),
    path(
        "generic/libro_crear",
        views.generic_libro_crear.as_view(),
        name="generic_libro_crear",
    ),
    path(
        "generic/libro_editar/<str:pk>",
        views.generic_libro_editar.as_view(),
        name="generic_libro_editar",
    ),
    path(
        "generic/libro_eliminar/<str:pk>",
        views.generic_libro_eliminar.as_view(),
        name="generic_libro_eliminar",
    ),
    path(
        "generic/libro_lista",
        views.generic_libro_lista.as_view(),
        name="generic_libro_lista",
    ),
    path(
        "generic/libro_detalles/<str:pk>",
        views.generic_libro_detalles.as_view(),
        name="generic_libro_detalles",
    ),
    path("8", views.template8, name="template8"),
    path("9", views.template9, name="template9"),
    path("10", views.template10, name="template10"),
]
