from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.http import HttpResponse
from rest_framework import generics
from . import models, forms, serializers

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

# Create your views here.
def login_view(request):
    if request.user.is_authenticated:
        return redirect("api_client")

    message = "Not Login"
    form = forms.LoginForm()

    if request.method == "POST":
        form = forms.LoginForm(request.POST or None)
        if form.is_valid():
            username = request.POST["username"]
            password = request.POST["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                message = "User logged"
                return redirect("api_client")
            else:
                message = "Username or password is not correct"
        else:
            message = "Invalid form"
    context = {"form": form, "message": message}
    return render(request, "login.html", context)


def logout_view(request):
    logout(request)
    return redirect("login")


def signup_view(request):
    if request.user.is_authenticated:
        return redirect("api_client")

    message = ""
    form = forms.SignupForm()

    if request.method == "POST":
        form = forms.SignupForm(request.POST or None)
        password = request.POST["password"]
        confirm_password = request.POST["confirm_password"]
        if password == confirm_password:
            if form.is_valid():
                user = User.objects.create_user(
                    username=request.POST["username"],
                    email=request.POST["email"],
                    first_name=request.POST["first_name"],
                    last_name=request.POST["last_name"],
                )
                user.set_password(password)
                user.save()
                user = authenticate(
                    request, username=request.POST["username"], password=password
                )
                if user is not None:
                    login(request, user)
                    return redirect("api_client")
                else:
                    message = "User not signup correctly"

            else:
                message = "Invalid form"
        else:
            message = "Passwords are not equals"
    context = {"form": form, "message": message}
    return render(request, "signup.html", context)


def api_client(request):
    return render(request, "home7/api-client.html", {"form": forms.LibroForm()})


def template1(request):
    return render(request, "home7/template-1.html", {})


def template2(request):
    return render(request, "home7/template-2.html", {})


def template3(request):
    return render(request, "home7/template-3.html", {})


def template4(request):
    return render(request, "home7/template-4.html", {})


def template5(request):
    return render(request, "home7/template-5.html", {})


def template6(request):
    return render(request, "home7/template-6.html", {})


def template7(request):
    return render(request, "home7/template-7.html", {})


def libro_crear(request):
    form = forms.LibroForm(request.POST or None)
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy("libro_lista"))
    contexto = {"forma": form}
    return render(request, "home7/libro-crear.html", contexto)


from django.views.generic.edit import CreateView


class generic_libro_crear(CreateView):
    model = models.Libro
    success_url = reverse_lazy("libro_lista")
    fields = forms.LibroForm.Meta.fields
    template_name = "home7/generic-libro-crear.html"


class libro_createview(generics.CreateAPIView):
    queryset = models.Libro.objects.all()
    serializer_class = serializers.LibroSerializer


def libro_editar(request, id):
    libro = models.Libro.objects.get(id=id)
    form = forms.LibroForm(request.POST or None, instance=libro)
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy("libro_lista"))
    contexto = {"forma": form}
    return render(request, "home7/libro-crear.html", contexto)


from django.views.generic.edit import UpdateView


class generic_libro_editar(UpdateView):
    model = models.Libro
    success_url = reverse_lazy("libro_lista")
    fields = forms.LibroForm.Meta.fields
    template_name = "home7/generic-libro-crear.html"


def libro_eliminar(request, id):
    libro = models.Libro.objects.get(id=id)
    if request.POST:
        libro.delete()
        return redirect(reverse_lazy("libro_lista"))
    contexto = {"libro": libro}
    return render(request, "home7/libro-eliminar.html", contexto)


from django.views.generic.edit import DeleteView


class generic_libro_eliminar(DeleteView):
    model = models.Libro
    success_url = reverse_lazy("libro_lista")
    template_name = "home7/generic-libro-eliminar.html"


def libro_lista(request):
    libros = models.Libro.objects.all()
    if request.POST:
        libros = libros.filter(id=request.POST.get("palabra"))
    contexto = {"libros": libros}
    return render(request, "home7/libro-lista.html", contexto)


from django.views.generic.list import ListView


class generic_libro_lista(ListView):
    model = models.Libro
    template_name = "home7/generic-libro-lista.html"


class libro_listview(generics.ListAPIView):
    queryset = models.Libro.objects.all()
    serializer_class = serializers.LibroSerializer


def libro_detalles(request, id):
    contexto = {"libro": models.Libro.objects.get(id=id)}
    return render(request, "home7/libro-detalles.html", contexto)


from django.views.generic.detail import DetailView


class generic_libro_detalles(DetailView):
    model = models.Libro
    template_name = "home7/generic-libro-detalles.html"


def template8(request):
    return render(request, "home7/template-8.html", {})


def template9(request):
    return render(request, "home7/template-9.html", {})


def template10(request):
    return render(request, "home7/template-10.html", {})


def random_libro(request):
    for _ in range(0, 5):
        libro = models.Libro()
        libro.random()
        libro.save()

    return HttpResponse(status=200)
