from django.db import models
import uuid
from django.core.validators import EmailValidator

email_validator = EmailValidator(
    whitelist="gmail", message="Must be gmail", code="invalid"
)


class CustomManager(models.Manager):
    def first_three(self):
        return super().get_queryset()[:3]

    def first_five(self):
        return super().get_queryset()[:5]


class Factura(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    rfc_emisor = models.CharField(max_length=30)
    rfc_receptor = models.CharField(max_length=30)
    correo_electronico = models.CharField(max_length=50, validators=[email_validator])
    concepto = models.CharField(max_length=50)
    subtotal = models.IntegerField()
    total = models.IntegerField()
    impuesto = models.IntegerField()
    tipo_factor = models.CharField(max_length=50)
    tasa_cuota = models.CharField(max_length=50)
    fecha = models.DateField()

    objects = CustomManager()

    def __str__(self):
        return "{0} {1} {2} {3}".format(
            self.id, self.fecha, self.rfc_emisor, self.rfc_receptor
        )

    def random(self):
        import rstr
        import datetime

        self.rfc_emisor = rstr.xeger(r"[A-Za-z0-9]{0,15}")
        self.rfc_receptor = rstr.xeger(r"[A-Za-z0-9]{0,15}")
        self.correo_electronico = rstr.xeger(r"[A-Za-z0-9]{0,15}@[A-Za-z]{0,10}\.com")
        self.concepto = rstr.xeger(r"[A-Za-z ]{0,30}")
        self.subtotal = int(str(rstr.xeger(r"[0-9]{1,3}")))
        self.total = int(str(rstr.xeger(r"[0-9]{1,3}")))
        self.impuesto = int(str(rstr.xeger(r"[0-9]{1,3}")))
        self.tipo_factor = rstr.xeger(r"[A-Za-z ]{0,10}")
        self.tipo_cuota = rstr.xeger(r"[A-Za-z ]{0,10}")
        self.fecha = datetime.date.today()


from django.db.models.signals import pre_save, pre_delete
from django.dispatch import receiver


@receiver(pre_save, sender=Factura)
def pre_save_handler(sender, instance, **kwargs):
    print("SAVING: {0}".format(instance))


@receiver(pre_delete, sender=Factura)
def pre_delete_handler(sender, instance, **kwargs):
    print("DELETING: {0}".format(instance))


pre_save.connect(pre_save_handler, sender=Factura)
pre_delete.connect(pre_delete_handler, sender=Factura)
