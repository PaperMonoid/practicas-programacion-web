from django.urls import path
from home3 import views

urlpatterns = [
    path("login", views.login_view, name="login"),
    path("logout", views.logout_view, name="logout"),
    path("signup", views.signup_view, name="signup"),
    path(
        "api/factura_listview",
        views.factura_listview.as_view(),
        name="api_factura_listview",
    ),
    path(
        "api/factura_createview",
        views.factura_createview.as_view(),
        name="api_factura_createview",
    ),
    path("api_client", views.api_client, name="api_client"),
    path("random_factura", views.random_factura, name="random_factura"),
    path("1", views.template1, name="template1"),
    path("2", views.template2, name="template2"),
    path("3", views.template3, name="template3"),
    path("factura_crear", views.factura_crear, name="factura_crear"),
    path("factura_editar/<str:id>", views.factura_editar, name="factura_editar"),
    path("factura_eliminar/<str:id>", views.factura_eliminar, name="factura_eliminar"),
    path("factura_lista", views.factura_lista, name="factura_lista"),
    path("factura_detalles/<str:id>", views.factura_detalles, name="factura_detalles"),
    path(
        "generic/factura_crear",
        views.generic_factura_crear.as_view(),
        name="generic_factura_crear",
    ),
    path(
        "generic/factura_editar/<str:pk>",
        views.generic_factura_editar.as_view(),
        name="generic_factura_editar",
    ),
    path(
        "generic/factura_eliminar/<str:pk>",
        views.generic_factura_eliminar.as_view(),
        name="generic_factura_eliminar",
    ),
    path(
        "generic/factura_lista",
        views.generic_factura_lista.as_view(),
        name="generic_factura_lista",
    ),
    path(
        "generic/factura_detalles/<str:pk>",
        views.generic_factura_detalles.as_view(),
        name="generic_factura_detalles",
    ),
    path("4", views.template4, name="template4"),
    path("5", views.template5, name="template5"),
    path("6", views.template6, name="template6"),
    path("7", views.template7, name="template7"),
    path("8", views.template8, name="template8"),
    path("9", views.template9, name="template9"),
    path("10", views.template10, name="template10"),
]
