from rest_framework import serializers
from . import models, forms


class FacturaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Factura
        fields = forms.FacturaForm.Meta.fields
