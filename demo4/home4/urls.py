from django.urls import path
from home4 import views

urlpatterns = [
    path("login", views.login_view, name="login"),
    path("logout", views.logout_view, name="logout"),
    path("signup", views.signup_view, name="signup"),
    path(
        "api/registro_listview",
        views.registro_listview.as_view(),
        name="api_registro_listview",
    ),
    path(
        "api/registro_createview",
        views.registro_createview.as_view(),
        name="api_registro_createview",
    ),
    path("api_client", views.api_client, name="api_client"),
    path(
        "random_registro_computadora",
        views.random_registro_computadora,
        name="random_registro_computadora",
    ),
    path("1", views.template1, name="template1"),
    path("2", views.template2, name="template2"),
    path("3", views.template3, name="template3"),
    path("4", views.template4, name="template4"),
    path("registro_crear", views.registro_crear, name="registro_crear"),
    path(
        "registro_editar/<str:numero_control>",
        views.registro_editar,
        name="registro_editar",
    ),
    path(
        "registro_eliminar/<str:numero_control>",
        views.registro_eliminar,
        name="registro_eliminar",
    ),
    path("registro_lista", views.registro_lista, name="registro_lista"),
    path(
        "registro_detalles/<str:numero_control>",
        views.registro_detalles,
        name="registro_detalles",
    ),
    path(
        "generic/registro_crear",
        views.generic_registro_crear.as_view(),
        name="generic_registro_crear",
    ),
    path(
        "generic/registro_editar/<str:pk>",
        views.generic_registro_editar.as_view(),
        name="generic_registro_editar",
    ),
    path(
        "generic/registro_eliminar/<str:pk>",
        views.generic_registro_eliminar.as_view(),
        name="generic_registro_eliminar",
    ),
    path(
        "generic/registro_lista",
        views.generic_registro_lista.as_view(),
        name="generic_registro_lista",
    ),
    path(
        "generic/registro_detalles/<str:pk>",
        views.generic_registro_detalles.as_view(),
        name="generic_registro_detalles",
    ),
    path("5", views.template5, name="template5"),
    path("6", views.template6, name="template6"),
    path("7", views.template7, name="template7"),
    path("8", views.template8, name="template8"),
    path("9", views.template9, name="template9"),
    path("10", views.template10, name="template10"),
]
