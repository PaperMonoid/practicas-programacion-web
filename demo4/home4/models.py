from django.db import models
import uuid
from django.core.validators import RegexValidator, EmailValidator

name_validator = RegexValidator(
    regex=r"[a-zA-Z ]+", message="Must be alphanumeric", code="invalid"
)
email_validator = EmailValidator(
    whitelist="gmail", message="Must be gmail", code="invalid"
)


class CustomManager(models.Manager):
    def first_three(self):
        return super().get_queryset()[:3]

    def first_five(self):
        return super().get_queryset()[:5]


class RegistroComputadora(models.Model):
    nombres = models.CharField(max_length=30, validators=[name_validator])
    apellidos = models.CharField(max_length=30, validators=[name_validator])
    correo_electronico = models.CharField(max_length=50, validators=[email_validator])
    numero_control = models.CharField(primary_key=True, max_length=50)
    numero_maquina = models.IntegerField()
    carrera = models.CharField(max_length=5)
    uso = models.CharField(max_length=15)
    dia = models.DateField()
    hora_entrada = models.TimeField()
    hora_salida = models.TimeField()

    objects = CustomManager()

    def __str__(self):
        return "{0} {1} {2}".format(self.dia, self.numero_maquina, self.numero_control)

    def random(self):
        import rstr
        import datetime

        self.nombres = rstr.xeger(r"[A-Za-z]{0,15}")
        self.apellidos = rstr.xeger(r"[A-Za-z]{0,15}")
        self.correo_electronico = rstr.xeger(r"[A-Za-z0-9]{0,15}@[A-Za-z]{0,10}\.com")
        self.numero_control = rstr.xeger(r"[0-9]{12}")
        self.numero_maquina = int(str(rstr.xeger(r"[0-9]{2}")))
        self.carrera = rstr.xeger(r"[A-Za-z]{3}")
        self.uso = rstr.xeger(r"[A-Za-z]{1,15}")
        self.dia = datetime.date.today()
        self.hora_entrada = datetime.datetime.now()
        self.hora_salida = datetime.datetime.now()


from django.db.models.signals import pre_save, pre_delete
from django.dispatch import receiver


@receiver(pre_save, sender=RegistroComputadora)
def pre_save_handler(sender, instance, **kwargs):
    print("SAVING: {0}".format(instance))


@receiver(pre_delete, sender=RegistroComputadora)
def pre_delete_handler(sender, instance, **kwargs):
    print("DELETING: {0}".format(instance))


pre_save.connect(pre_save_handler, sender=RegistroComputadora)
pre_delete.connect(pre_delete_handler, sender=RegistroComputadora)
