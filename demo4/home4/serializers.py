from rest_framework import serializers
from . import models, forms


class RegistroComputadoraSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.RegistroComputadora
        fields = forms.RegistroComputadoraForm.Meta.fields
