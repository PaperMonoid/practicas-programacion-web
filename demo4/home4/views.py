from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.http import HttpResponse
from rest_framework import generics
from . import models, forms, serializers

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

# Create your views here.
def login_view(request):
    if request.user.is_authenticated:
        return redirect("api_client")

    message = "Not Login"
    form = forms.LoginForm()

    if request.method == "POST":
        form = forms.LoginForm(request.POST or None)
        if form.is_valid():
            username = request.POST["username"]
            password = request.POST["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                message = "User logged"
                return redirect("api_client")
            else:
                message = "Username or password is not correct"
        else:
            message = "Invalid form"
    context = {"form": form, "message": message}
    return render(request, "login.html", context)


def logout_view(request):
    logout(request)
    return redirect("login")


def signup_view(request):
    if request.user.is_authenticated:
        return redirect("api_client")

    message = ""
    form = forms.SignupForm()

    if request.method == "POST":
        form = forms.SignupForm(request.POST or None)
        password = request.POST["password"]
        confirm_password = request.POST["confirm_password"]
        if password == confirm_password:
            if form.is_valid():
                user = User.objects.create_user(
                    username=request.POST["username"],
                    email=request.POST["email"],
                    first_name=request.POST["first_name"],
                    last_name=request.POST["last_name"],
                )
                user.set_password(password)
                user.save()
                user = authenticate(
                    request, username=request.POST["username"], password=password
                )
                if user is not None:
                    login(request, user)
                    return redirect("api_client")
                else:
                    message = "User not signup correctly"

            else:
                message = "Invalid form"
        else:
            message = "Passwords are not equals"
    context = {"form": form, "message": message}
    return render(request, "signup.html", context)


def api_client(request):
    return render(
        request, "home4/api-client.html", {"form": forms.RegistroComputadoraForm()}
    )


def template1(request):
    return render(request, "home4/template-1.html", {})


def template2(request):
    return render(request, "home4/template-2.html", {})


def template3(request):
    return render(request, "home4/template-3.html", {})


def template4(request):
    return render(request, "home4/template-4.html", {})


def registro_crear(request):
    form = forms.RegistroComputadoraForm(request.POST or None)
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy("registro_lista"))
    contexto = {"forma": form}
    return render(request, "home4/registro-crear.html", contexto)


from django.views.generic.edit import CreateView


class generic_registro_crear(CreateView):
    model = models.RegistroComputadora
    success_url = reverse_lazy("registro_lista")
    fields = forms.RegistroComputadoraForm.Meta.fields
    template_name = "home4/generic-registro-crear.html"


class registro_createview(generics.CreateAPIView):
    queryset = models.RegistroComputadora.objects.all()
    serializer_class = serializers.RegistroComputadoraSerializer


def registro_editar(request, numero_control):
    registro = models.RegistroComputadora.objects.get(numero_control=numero_control)
    form = forms.RegistroComputadoraForm(request.POST or None, instance=registro)
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy("registro_lista"))
    contexto = {"forma": form}
    return render(request, "home4/registro-crear.html", contexto)


from django.views.generic.edit import UpdateView


class generic_registro_editar(UpdateView):
    model = models.RegistroComputadora
    success_url = reverse_lazy("registro_lista")
    fields = forms.RegistroComputadoraForm.Meta.fields
    template_name = "home4/generic-registro-crear.html"


def registro_eliminar(request, numero_control):
    registro = models.RegistroComputadora.objects.get(numero_control=numero_control)
    if request.POST:
        registro.delete()
        return redirect(reverse_lazy("registro_lista"))
    contexto = {"registros": registro}
    return render(request, "home4/registro-eliminar.html", contexto)


from django.views.generic.edit import DeleteView


class generic_registro_eliminar(DeleteView):
    model = models.RegistroComputadora
    success_url = reverse_lazy("registro_lista")
    template_name = "home4/generic-registro-eliminar.html"


def registro_lista(request):
    registros = models.RegistroComputadora.objects.all()
    if request.POST:
        registros = registros.filter(numero_control=request.POST.get("palabra"))
    contexto = {"registros": registros}
    return render(request, "home4/registro-lista.html", contexto)


from django.views.generic.list import ListView


class generic_registro_lista(ListView):
    model = models.RegistroComputadora
    template_name = "home4/generic-registro-lista.html"


class registro_listview(generics.ListAPIView):
    queryset = models.RegistroComputadora.objects.all()
    serializer_class = serializers.RegistroComputadoraSerializer


def registro_detalles(request, numero_control):
    contexto = {
        "registro": models.RegistroComputadora.objects.get(
            numero_control=numero_control
        )
    }
    return render(request, "home4/registro-detalles.html", contexto)


from django.views.generic.detail import DetailView


class generic_registro_detalles(DetailView):
    model = models.RegistroComputadora
    template_name = "home4/generic-registro-detalles.html"


def template5(request):
    return render(request, "home4/template-5.html", {})


def template6(request):
    return render(request, "home4/template-6.html", {})


def template7(request):
    return render(request, "home4/template-7.html", {})


def template8(request):
    return render(request, "home4/template-8.html", {})


def template9(request):
    return render(request, "home4/template-9.html", {})


def template10(request):
    return render(request, "home4/template-10.html", {})


def random_registro_computadora(request):
    for _ in range(0, 5):
        registro = models.RegistroComputadora()
        registro.random()
        registro.save()

    return HttpResponse(status=200)
