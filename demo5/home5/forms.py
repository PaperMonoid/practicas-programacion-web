from django.forms import ModelForm
from django import forms
from . import models


class LoginForm(forms.Form):
    username = forms.CharField(
        max_length=24, widget=forms.TextInput(attrs={"class": "input"})
    )
    password = forms.CharField(
        max_length=24, widget=forms.PasswordInput(attrs={"class": "input"})
    )


class SignupForm(forms.Form):
    first_name = forms.CharField(
        max_length=24, widget=forms.TextInput(attrs={"class": "input"})
    )
    last_name = forms.CharField(
        max_length=24, widget=forms.TextInput(attrs={"class": "input"})
    )
    email = forms.EmailField(widget=forms.EmailInput(attrs={"class": "input"}))
    username = forms.CharField(
        max_length=24, widget=forms.TextInput(attrs={"class": "input"})
    )
    password = forms.CharField(
        max_length=24, widget=forms.PasswordInput(attrs={"class": "input"})
    )
    confirm_password = forms.CharField(
        max_length=24, widget=forms.PasswordInput(attrs={"class": "input"})
    )


class NumeroPrimoForm(ModelForm):
    class Meta:
        model = models.NumeroPrimo
        fields = [
            "numero",
            "descripcion_matematica",
            "comentario",
            "codigo_prueba",
            "digitos",
            "fecha",
            "tipo",
        ]
