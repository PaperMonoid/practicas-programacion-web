from rest_framework import serializers
from . import models, forms


class NumeroPrimoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.NumeroPrimo
        fields = forms.NumeroPrimoForm.Meta.fields
