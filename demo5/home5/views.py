from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.http import HttpResponse
from rest_framework import generics
from . import models, forms, serializers

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

# Create your views here.
def login_view(request):
    if request.user.is_authenticated:
        return redirect("api_client")

    message = "Not Login"
    form = forms.LoginForm()

    if request.method == "POST":
        form = forms.LoginForm(request.POST or None)
        if form.is_valid():
            username = request.POST["username"]
            password = request.POST["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                message = "User logged"
                return redirect("api_client")
            else:
                message = "Username or password is not correct"
        else:
            message = "Invalid form"
    context = {"form": form, "message": message}
    return render(request, "login.html", context)


def logout_view(request):
    logout(request)
    return redirect("login")


def signup_view(request):
    if request.user.is_authenticated:
        return redirect("api_client")

    message = ""
    form = forms.SignupForm()

    if request.method == "POST":
        form = forms.SignupForm(request.POST or None)
        password = request.POST["password"]
        confirm_password = request.POST["confirm_password"]
        if password == confirm_password:
            if form.is_valid():
                user = User.objects.create_user(
                    username=request.POST["username"],
                    email=request.POST["email"],
                    first_name=request.POST["first_name"],
                    last_name=request.POST["last_name"],
                )
                user.set_password(password)
                user.save()
                user = authenticate(
                    request, username=request.POST["username"], password=password
                )
                if user is not None:
                    login(request, user)
                    return redirect("api_client")
                else:
                    message = "User not signup correctly"

            else:
                message = "Invalid form"
        else:
            message = "Passwords are not equals"
    context = {"form": form, "message": message}
    return render(request, "signup.html", context)


def api_client(request):
    return render(request, "home5/api-client.html", {"form": forms.NumeroPrimoForm()})


def template1(request):
    return render(request, "home5/template-1.html", {})


def template2(request):
    return render(request, "home5/template-2.html", {})


def template3(request):
    return render(request, "home5/template-3.html", {})


def template4(request):
    return render(request, "home5/template-4.html", {})


def template5(request):
    return render(request, "home5/template-5.html", {})


def primos_crear(request):
    form = forms.NumeroPrimoForm(request.POST or None)
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy("primos_lista"))
    contexto = {"forma": form}
    return render(request, "home5/primos-crear.html", contexto)


from django.views.generic.edit import CreateView


class generic_primos_crear(CreateView):
    model = models.NumeroPrimo
    success_url = reverse_lazy("primo_lista")
    fields = forms.NumeroPrimoForm.Meta.fields
    template_name = "home5/generic-primos-crear.html"


class primos_createview(generics.CreateAPIView):
    queryset = models.NumeroPrimo.objects.all()
    serializer_class = serializers.NumeroPrimoSerializer


def primos_editar(request, numero):
    numero_primo = models.NumeroPrimo.objects.get(numero=numero)
    form = forms.NumeroPrimoForm(request.POST or None, instance=numero_primo)
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy("primos_lista"))
    contexto = {"forma": form}
    return render(request, "home5/primos-crear.html", contexto)


from django.views.generic.edit import UpdateView


class generic_primos_editar(UpdateView):
    model = models.NumeroPrimo
    success_url = reverse_lazy("primo_lista")
    fields = forms.NumeroPrimoForm.Meta.fields
    template_name = "home5/generic-primos-crear.html"


def primos_eliminar(request, numero):
    numero_primo = models.NumeroPrimo.objects.get(numero=numero)
    if request.POST:
        numero_primo.delete()
        return redirect(reverse_lazy("primos_lista"))
    contexto = {"primo": numero_primo}
    return render(request, "home5/primos-eliminar.html", contexto)


from django.views.generic.edit import DeleteView


class generic_primos_eliminar(DeleteView):
    model = models.NumeroPrimo
    success_url = reverse_lazy("primo_lista")
    template_name = "home5/generic-primos-eliminar.html"


def primos_lista(request):
    primos = models.NumeroPrimo.objects.all()
    if request.POST:
        primos = primos.filter(numero=request.POST.get("palabra"))
    contexto = {"primos": primos}
    return render(request, "home5/primos-lista.html", contexto)


from django.views.generic.list import ListView


class generic_primos_lista(ListView):
    model = models.NumeroPrimo
    template_name = "home5/generic-primos-lista.html"


class primos_listview(generics.ListAPIView):
    queryset = models.NumeroPrimo.objects.all()
    serializer_class = serializers.NumeroPrimoSerializer


def primos_detalles(request, numero):
    contexto = {"primo": models.NumeroPrimo.objects.get(numero=numero)}
    return render(request, "home5/primos-detalles.html", contexto)


from django.views.generic.detail import DetailView


class generic_primos_detalles(DetailView):
    model = models.NumeroPrimo
    template_name = "home5/generic-primos-detalles.html"


def template6(request):
    return render(request, "home5/template-6.html", {})


def template7(request):
    return render(request, "home5/template-7.html", {})


def template8(request):
    return render(request, "home5/template-8.html", {})


def template9(request):
    return render(request, "home5/template-9.html", {})


def template10(request):
    return render(request, "home5/template-10.html", {})


def random_numero_primo(request):
    for _ in range(0, 5):
        numero_primo = models.NumeroPrimo()
        numero_primo.random()
        numero_primo.save()
    return HttpResponse(status=200)
