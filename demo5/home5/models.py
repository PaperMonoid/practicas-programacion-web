from django.db import models
import uuid
from django.core.validators import RegexValidator

name_validator = RegexValidator(
    regex=r"[a-zA-Z ]+", message="Must be alphanumeric", code="invalid"
)


class CustomManager(models.Manager):
    def first_three(self):
        return super().get_queryset()[:3]

    def first_five(self):
        return super().get_queryset()[:5]


class NumeroPrimo(models.Model):
    numero = models.IntegerField(primary_key=True)
    descripcion_matematica = models.CharField(max_length=100)
    comentario = models.CharField(max_length=50, validators=[name_validator])
    codigo_prueba = models.CharField(max_length=3)
    digitos = models.IntegerField()
    fecha = models.DateField()
    tipo = models.CharField(max_length=15)

    objects = CustomManager()

    def __str__(self):
        return "{0} {1}".format(self.fecha, self.descripcion_matematica)

    def random(self):
        import rstr
        import datetime

        self.numero = int(str(rstr.xeger(r"[0-9]{1,10}")))
        self.descripcion_matematica = rstr.xeger(r"[0-9]{1,5}\*10\^[0-9]{1,5}\+1")
        self.comentario = rstr.xeger(r"[A-Za-z]{0,30}")
        self.codigo_prueba = rstr.xeger(r"C|(D\*)")
        self.digitos = int(str(rstr.xeger(r"[1-9]")))
        self.fecha = datetime.date.today()
        self.tipo = rstr.xeger(r"(verificado)|(no verificado)")


from django.db.models.signals import pre_save, pre_delete
from django.dispatch import receiver


@receiver(pre_save, sender=NumeroPrimo)
def pre_save_handler(sender, instance, **kwargs):
    print("SAVING: {0}".format(instance))


@receiver(pre_delete, sender=NumeroPrimo)
def pre_delete_handler(sender, instance, **kwargs):
    print("DELETING: {0}".format(instance))


pre_save.connect(pre_save_handler, sender=NumeroPrimo)
pre_delete.connect(pre_delete_handler, sender=NumeroPrimo)
