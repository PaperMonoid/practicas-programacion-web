from django.urls import path
from home5 import views

urlpatterns = [
    path("login", views.login_view, name="login"),
    path("logout", views.logout_view, name="logout"),
    path("signup", views.signup_view, name="signup"),
    path(
        "api/primos_listview",
        views.primos_listview.as_view(),
        name="api_primos_listview",
    ),
    path(
        "api/primos_createview",
        views.primos_createview.as_view(),
        name="api_primos_createview",
    ),
    path("api_client", views.api_client, name="api_client"),
    path("random_numero_primo", views.random_numero_primo, name="random_numero_primo"),
    path("1", views.template1, name="template1"),
    path("1", views.template1, name="template1"),
    path("2", views.template2, name="template2"),
    path("3", views.template3, name="template3"),
    path("4", views.template4, name="template4"),
    path("5", views.template5, name="template5"),
    path("primos_crear", views.primos_crear, name="primos_crear"),
    path("primos_editar/<str:numero>", views.primos_editar, name="primos_editar"),
    path("primos_eliminar/<str:numero>", views.primos_eliminar, name="primos_eliminar"),
    path("primos_lista", views.primos_lista, name="primos_lista"),
    path("primos_detalles/<str:numero>", views.primos_detalles, name="primos_detalles"),
    path(
        "generic/primos_crear",
        views.generic_primos_crear.as_view(),
        name="generic_primos_crear",
    ),
    path(
        "generic/primos_editar/<str:pk>",
        views.generic_primos_editar.as_view(),
        name="generic_primos_editar",
    ),
    path(
        "generic/primos_eliminar/<str:pk>",
        views.generic_primos_eliminar.as_view(),
        name="generic_primos_eliminar",
    ),
    path(
        "generic/primos_lista",
        views.generic_primos_lista.as_view(),
        name="generic_primos_lista",
    ),
    path(
        "generic/primos_detalles/<str:pk>",
        views.generic_primos_detalles.as_view(),
        name="generic_primos_detalles",
    ),
    path("6", views.template6, name="template6"),
    path("7", views.template7, name="template7"),
    path("8", views.template8, name="template8"),
    path("9", views.template9, name="template9"),
    path("10", views.template10, name="template10"),
]
